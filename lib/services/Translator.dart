import 'package:flutter_hack19/models/language.dart';
import 'package:translator/translator.dart';

class Translator {
  static final Translator shared = Translator();
  final translator = GoogleTranslator();

  Future<String> translate(String text, Language language) async {
    final String translatedText =
        await translator.translate(text, to: language.code);
    return translatedText ?? text;
  }
}
