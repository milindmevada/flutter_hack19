import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_hack19/models/user.dart';

class UserService {
  static Future addUser(User user) {
    return Firestore.instance
        .collection('user')
        .document()
        .setData(user.toJson());
  }
}
