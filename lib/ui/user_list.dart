import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hack19/blocs/chat_messages_bloc.dart';
import 'package:flutter_hack19/models/user.dart';
import 'package:flutter_hack19/ui/chat_screen.dart';
import 'package:provider/provider.dart';

class UsersListWidget extends StatelessWidget {
  final User user;

  const UsersListWidget({this.user});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Users'),
      ),
      body: _AllUsersList(loggedInUser: user),
    );
  }
}

class _AllUsersList extends StatelessWidget {
  final User loggedInUser;

  const _AllUsersList({this.loggedInUser});

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: Firestore.instance.collection('user').snapshots(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasError) return Text('Error: ${snapshot.error}');
        switch (snapshot.connectionState) {
          case ConnectionState.waiting:
            return Text('Loading...');
          default:
            return ListView(
              children:
                  snapshot.data.documents.map((DocumentSnapshot document) {
                User user = User.fromJson(document.data);
                if (user.email == loggedInUser.email) {
                  return Container();
                }
                return ListTile(
                  leading: CircleAvatar(
                    radius: 30.0,
                    backgroundColor: Colors.blue,
                  ),
                  title: Text(user.email),
                  subtitle: Text(user.language.name),
                  onTap: () {
                    final route = MaterialPageRoute(
                      builder: (_) => Provider<ChatMessagesBLoC>(
                            builder: (context) => ChatMessagesBLoC(loggedInUser,user),
                            child: ChatScreenWidget(),
                          ),
                    );
                    Navigator.of(context).push(route);
                  },
                );
              }).toList(),
            );
        }
      },
    );
  }
}
