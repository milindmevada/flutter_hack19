import 'package:flutter/material.dart';
import 'package:flutter_hack19/blocs/chat_messages_bloc.dart';
import 'package:flutter_hack19/models/message.dart';
import 'package:provider/provider.dart';

class ChatScreenWidget extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('User Name'),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Expanded(
              child: _ChatList(),
            ),
            _MessageInput(),
            SizedBox(
              height: MediaQuery.of(context).padding.bottom,
            )
          ],
        ),
      ),
    );
  }
}

class _ChatList extends StatefulWidget {
  @override
  __ChatListState createState() => __ChatListState();
}

class __ChatListState extends State<_ChatList> {
  @override
  Widget build(BuildContext context) {
    final bloc = Provider.of<ChatMessagesBLoC>(context);

    return StreamBuilder<List<Message>>(
      stream: bloc.getMessages(),
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }

        final messages = snapshot.data;

        return ListView.builder(
          itemBuilder: (_, index) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Text(
                  messages[index].message,
                  textAlign: index % 2 == 0 ? TextAlign.end : TextAlign.start,
                ),
              ],
            );
          },
          itemCount: messages.length,
        );
      },
    );
  }
}

class _MessageInput extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bloc = Provider.of<ChatMessagesBLoC>(context);
    final controller = TextEditingController();

    return Row(
      children: <Widget>[
        Expanded(
          child: TextField(
            controller: controller,
            keyboardType: TextInputType.text,
            textInputAction: TextInputAction.send,
          ),
        ),
        IconButton(
          icon: Icon(Icons.send),
          onPressed: () {
            bloc.sendMessage(controller.text);
          },
        )
      ],
    );
  }
}
