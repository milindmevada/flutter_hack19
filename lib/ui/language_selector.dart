import 'package:flutter/material.dart';
import 'package:flutter_hack19/blocs/language_selector_bloc.dart';
import 'package:flutter_hack19/models/language.dart';
import 'package:provider/provider.dart';

class LanguageSelectorWidget extends StatelessWidget {
  final Function(Language language) onSelect;
  const LanguageSelectorWidget({@required this.onSelect});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Select your Language'),
      ),
      body: Provider<LanguageSelectorBLoC>(
        builder: (_) => LanguageSelectorBLoC(),
        dispose: (_, bloc) => bloc.dispose(),
        child: _LanguageList(onSelect: onSelect),
      ),
    );
  }
}

class _LanguageList extends StatelessWidget {
  final Function(Language language) onSelect;

  const _LanguageList({this.onSelect});
  @override
  Widget build(BuildContext context) {
    final bloc = Provider.of<LanguageSelectorBLoC>(context);

    return StreamBuilder<List<Language>>(
        stream: bloc.languageList,
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }

          final languageList = snapshot.data;

          return ListView.builder(
            itemBuilder: (context, index) {
              return ListTile(
                title: Text(languageList[index].name),
                subtitle: Text(languageList[index].code),
                onTap: () {
                  onSelect(languageList[index]);
                  Navigator.of(context).pop();
                },
              );
            },
            itemCount: languageList.length,
          );
        });
  }
}
