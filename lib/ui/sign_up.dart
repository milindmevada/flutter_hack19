import 'package:flutter/material.dart';
import 'package:flutter_hack19/blocs/sign_up_bloc.dart';
import 'package:flutter_hack19/models/language.dart';
import 'package:flutter_hack19/models/user.dart';
import 'package:flutter_hack19/services/UserService.dart';
import 'package:flutter_hack19/ui/language_selector.dart';
import 'package:flutter_hack19/ui/user_list.dart';
import 'package:provider/provider.dart';

class SignUpWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bloc = Provider.of<SignUpBloc>(context);

    final TextEditingController emailController = TextEditingController();

    return Scaffold(
      appBar: AppBar(
        title: Text('Signup'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          TextField(
            keyboardType: TextInputType.emailAddress,
            controller: emailController,
          ),
          RaisedButton(
            child: Text(
              'Get Started',
            ),
            onPressed: () => bloc.onSignUpClick.add(null),
          ),
          StreamBuilder<bool>(
            stream: bloc.isUserSignedUp,
            builder: (context, signUpSnapShot) {
              if (!signUpSnapShot.hasData) {
                return Container();
              }

              if (!signUpSnapShot.data) {
                return RaisedButton(
                  child: Text(
                    'Select your Language',
                  ),
                  onPressed: () {
                    Language selectedLanguage;
                    final route = MaterialPageRoute(
                        builder: (_) => LanguageSelectorWidget(
                              onSelect: (Language language) {
                                selectedLanguage = language;
                              },
                            ));
                    Navigator.of(context).push(route).then((_) {
                      final User user = User(
                          email: emailController.text.toLowerCase(),
                          language: selectedLanguage);
                      UserService.addUser(user).then((callback) {
                        user.save();
                        final route = MaterialPageRoute(
                            builder: (_) => UsersListWidget(user: user));
                        Navigator.of(context).pushReplacement(route);
                      });
                    });
                  },
                );
              } else {
                return Container();
              }
            },
          )
        ],
      ),
    );
  }
}
