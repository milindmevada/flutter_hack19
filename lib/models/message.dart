class Message {
  String senderId;
  String receiverId;
  String message;
  int createdAt;
  Map<String, String> translations;

  Message({
    this.senderId,
    this.receiverId,
    this.createdAt,
    this.translations,
    this.message,
  });

  factory Message.fromMap(Map<String, dynamic> json) => new Message(
        senderId: json["sender_id"],
        receiverId: json["receiver_id"],
        createdAt: json["created_at"],
        translations: json["translations"],
        message: json["message"],
      );

  Map<String, dynamic> toMap() => {
        "sender_id": senderId,
        "receiver_id": receiverId,
        "created_at": createdAt,
        "message": message,
        "translations": translations,
      };
}
