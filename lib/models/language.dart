import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

class Language {
  final String name;
  final String code;
  Language({this.name, this.code});

  factory Language.fromJson(Map<String, dynamic> json) {
    if (json == null) {
      throw FormatException("Null JSON provided to SimpleObject");
    }

    return Language(name: json['name'], code: json['code']);
  }

  String jsonString() {
    final Map<String, String> hashMap = {'name': name, 'code': code};
    final jsonString = json.encode(hashMap);
    return jsonString;
  }

  save() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('userLanguge', jsonString());
  }

  static Future<Language> userPreferedLanguge() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String languageString = prefs.getString('userLanguge');
    var jsonObj = json.decode(languageString);
    return Language.fromJson(jsonObj);
  }

  static final List<Language> allLanguages = [
    Language(name: 'Afrikaans', code: 'af'),
    Language(name: 'Afrikaans', code: 'af'),
    Language(name: 'Albanian', code: 'sq'),
    Language(name: 'Amharic', code: 'am'),
    Language(name: 'Arabic', code: 'ar'),
    Language(name: 'Armenian', code: 'hy'),
    Language(name: 'Azerbaijani', code: 'az'),
    Language(name: 'Basque', code: 'eu'),
    Language(name: 'Belarusian', code: 'be'),
    Language(name: 'Bengali', code: 'bn'),
    Language(name: 'Bosnian', code: 'bs'),
    Language(name: 'Bulgarian', code: 'bg'),
    Language(name: 'Catalan', code: 'ca'),
    Language(name: 'Cebuano', code: 'ceb'),
    Language(name: 'Chinese', code: 'zh'),
    Language(name: 'Chinese', code: 'zh'),
    Language(name: 'Corsican', code: 'co'),
    Language(name: 'Croatian', code: 'hr'),
    Language(name: 'Czech', code: 'cs'),
    Language(name: 'Danish', code: 'da'),
    Language(name: 'Dutch', code: 'nl'),
    Language(name: 'English', code: 'en'),
    Language(name: 'Esperanto', code: 'eo'),
    Language(name: 'Estonian', code: 'et'),
    Language(name: 'Finnish', code: 'fi'),
    Language(name: 'French', code: 'fr'),
    Language(name: 'Frisian', code: 'fy'),
    Language(name: 'Galician', code: 'gl'),
    Language(name: 'Georgian', code: 'ka'),
    Language(name: 'German', code: 'de'),
    Language(name: 'Greek', code: 'el'),
    Language(name: 'Gujarati', code: 'gu'),
    Language(name: 'Haitian Creole', code: 'ht'),
    Language(name: 'Hausa', code: 'ha'),
    Language(name: 'Hawaiian', code: 'haw'),
    Language(name: 'Hebrew', code: 'he'),
    Language(name: 'Hindi', code: 'hi'),
    Language(name: 'Hmong', code: 'hmn'),
    Language(name: 'Hungarian', code: 'hu'),
    Language(name: 'Icelandic', code: 'is'),
    Language(name: 'Igbo', code: 'ig'),
    Language(name: 'Indonesian', code: 'id'),
    Language(name: 'Irish', code: 'ga'),
    Language(name: 'Italian', code: 'it'),
    Language(name: 'Japanese', code: 'ja'),
    Language(name: 'Javanese', code: 'jw'),
    Language(name: 'Kannada', code: 'kn'),
    Language(name: 'Kazakh', code: 'kk'),
    Language(name: 'Khmer', code: 'km'),
    Language(name: 'Korean', code: 'ko'),
    Language(name: 'Kurdish', code: 'ku'),
    Language(name: 'Kyrgyz', code: 'ky'),
    Language(name: 'Lao', code: 'lo'),
    Language(name: 'Latin', code: 'la'),
    Language(name: 'Latvian', code: 'lv'),
    Language(name: 'Lithuanian', code: 'lt'),
    Language(name: 'Luxembourgish', code: 'lb'),
    Language(name: 'Macedonian', code: 'mk'),
    Language(name: 'Malagasy', code: 'mg'),
    Language(name: 'Malay', code: 'ms'),
    Language(name: 'Malayalam', code: 'ml'),
    Language(name: 'Maltese', code: 'mt'),
    Language(name: 'Maori', code: 'mi'),
    Language(name: 'Marathi', code: 'mr'),
    Language(name: 'Mongolian', code: 'mn'),
    Language(name: 'Myanmar', code: 'my'),
    Language(name: 'Nepali', code: 'ne'),
    Language(name: 'Norwegian', code: 'no'),
    Language(name: 'Nyanja', code: 'ny'),
    Language(name: 'Pashto', code: 'ps'),
    Language(name: 'Persian', code: 'fa'),
    Language(name: 'Polish', code: 'pl'),
    Language(name: 'Portuguese', code: 'pt'),
    Language(name: 'Punjabi', code: 'pa'),
    Language(name: 'Romanian', code: 'ro'),
    Language(name: 'Russian', code: 'ru'),
    Language(name: 'Samoan', code: 'sm'),
    Language(name: 'Scots Gaelic', code: 'gd'),
    Language(name: 'Serbian', code: 'sr'),
    Language(name: 'Sesotho', code: 'st'),
    Language(name: 'Shona', code: 'sn'),
    Language(name: 'Sindhi', code: 'sd'),
    Language(name: 'Sinhala', code: 'si'),
    Language(name: 'Slovak', code: 'sk'),
    Language(name: 'Slovenian', code: 'sl'),
    Language(name: 'Somali', code: 'so'),
    Language(name: 'Spanish', code: 'es'),
    Language(name: 'Sundanese', code: 'su'),
    Language(name: 'Swahili', code: 'sw'),
    Language(name: 'Swedish', code: 'sv'),
    Language(name: 'Tagalog', code: 'tl'),
    Language(name: 'Tajik', code: 'tg'),
    Language(name: 'Tamil', code: 'ta'),
    Language(name: 'Telugu', code: 'te'),
    Language(name: 'Thai', code: 'th'),
    Language(name: 'Turkish', code: 'tr'),
    Language(name: 'Ukrainian', code: 'uk'),
    Language(name: 'Urdu', code: 'ur'),
    Language(name: 'Uzbek', code: 'uz'),
    Language(name: 'Vietnamese', code: 'vi'),
    Language(name: 'Welsh', code: 'cy'),
    Language(name: 'Xhosa', code: 'xh'),
    Language(name: 'Yiddish', code: 'yi'),
    Language(name: 'Yoruba', code: 'yo'),
    Language(name: 'Zulu', code: 'zu'),
  ];
}
