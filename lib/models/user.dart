import 'dart:convert';

import 'package:flutter_hack19/models/language.dart';
import 'package:shared_preferences/shared_preferences.dart';

class User {
  String name;
  String email;
  Language language;

  User({
    this.name,
    this.email,
    this.language
  });

  factory User.fromJson(Map<String, dynamic> json) {
    User user = User(
      name: json["name"],
      email: json["email"],
    );
    String lanCode = json["language"];
    user.language =
        Language.allLanguages.firstWhere((lng) => lng.code == lanCode);
    return user;
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> map = {
      "name": name ?? "",
      "email": email ?? "",
    };
    map['language'] = language?.code ?? "";
    return map;
  }

  save() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final Map<String, dynamic> hashMap = toJson();
    final jsonString = json.encode(hashMap);
    prefs.setString('loggedInUser', jsonString);
  }

  static Future<User> loggedInUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String languageString = prefs.getString('loggedInUser');
    if (languageString == null) {
      return null;
    }
    var jsonObj = json.decode(languageString);
    return User.fromJson(jsonObj);
  }
}
