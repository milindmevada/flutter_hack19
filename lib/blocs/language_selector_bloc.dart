import 'package:flutter_hack19/models/language.dart';
import 'package:rxdart/rxdart.dart';

class LanguageSelectorBLoC {
  final languageList = BehaviorSubject<List<Language>>.seeded([]);

  LanguageSelectorBLoC() {
    languageList.add(Language.allLanguages);
  }

  void dispose() {
    languageList.close();
  }
}
