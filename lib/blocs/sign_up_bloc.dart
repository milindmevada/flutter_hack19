import 'package:rxdart/rxdart.dart';

class SignUpBloc {
  final isUserSignedUp = BehaviorSubject<bool>();
  final onSignUpClick = PublishSubject<void>();

  SignUpBloc() {
    onSignUpClick.listen(
      (_) => isUserSignedUp.add(false),
    );
  }

  void dispose() {
    isUserSignedUp.close();
    onSignUpClick.close();
  }
}
