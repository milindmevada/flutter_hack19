import 'package:rxdart/rxdart.dart';

class ChatBLoC {
  final messages = BehaviorSubject<List<String>>.seeded([]);
  final newMsg = PublishSubject<void>();

  ChatBLoC() {
    newMsg.delay(Duration(seconds: 1)).map((_) {
      final existingList = messages.value;
      existingList.add('From other end');
      messages.add(existingList);
    }).listen(null);
  }

  void dispose() {
    messages.close();
    newMsg.close();
  }
}
