import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_hack19/models/message.dart';
import 'package:flutter_hack19/models/user.dart';
import 'package:flutter_hack19/services/translator.dart';
import 'package:rxdart/rxdart.dart';

class ChatMessagesBLoC {
  final User _currentUser;
  final User _receiver;
  ChatMessagesBLoC(this._currentUser, this._receiver);

  Observable<List<Message>> getMessages() {
    final chatId = _getChatID(_currentUser, _receiver);

    return Observable(Firestore.instance.collection(chatId).snapshots())
        .map((snap) {
      return snap.documents.map((doc) => Message.fromMap(doc.data));
    });
  }

  Future<Message> sendMessage(String msg) async {
    String translatedMsg;
    if (_currentUser.language != _receiver.language) {
      translatedMsg =
          await Translator.shared.translate(msg, _receiver.language);
    }

    final Map<String, String> translations = {};
    if (translatedMsg != null) {
      translations[_receiver.language.code] = translatedMsg;
    }

    final message = Message(
      senderId: _currentUser.email,
      receiverId: _receiver.email,
      message: msg,
      createdAt: DateTime.now().millisecondsSinceEpoch,
      translations: translations,
    );

    final chatId = _getChatID(_currentUser, _receiver);
    await Firestore.instance.collection(chatId).add(message.toMap());
    return message;
  }
}

String _getChatID(User current, User receiver) {
  final senderID = current.email.hashCode;
  final receiverID = receiver.email.hashCode;
  final list = [senderID, receiverID];
  list.sort((a, b) => a.compareTo(b));
  return list.join("_");
}
